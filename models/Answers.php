<?php

class Answers
{
    public static function addAnswerItem($survey_id, $question_id, $answer_id)
    {

        if (isset($_SESSION['username'])) {
            $db = Db::getConnection();

            //check if question has answer
            $sql = 'SELECT count(user_survey_id) as count FROM user_surveys
                      WHERE username=:username AND survey_id=:survey_id AND question_id=:question_id';

            $result = $db->prepare($sql);
            $result->bindParam(':username', $_SESSION['username'], PDO::PARAM_STR);
            $result->bindParam(':question_id', $question_id, PDO::PARAM_INT);
            $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
            $result->execute();

            $row = $result->fetch();

            //need for JS
            echo $row['count'];

            if ($row['count'] == 0) {
                $correct_f = self::isCorrectAnswer($survey_id, $question_id, $answer_id);

                $sql = 'INSERT IGNORE INTO user_surveys(username, survey_id, question_id, answer_id, correct_f) 
                      VALUES(:username, :survey_id, :question_id, :answer_id, :correct_f)';

                $result = $db->prepare($sql);
                $result->bindParam(':username', $_SESSION['username'], PDO::PARAM_STR);
                $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
                $result->bindParam(':question_id', $question_id, PDO::PARAM_INT);
                $result->bindParam(':answer_id', $answer_id, PDO::PARAM_INT);
                $result->bindParam(':correct_f', $correct_f, PDO::PARAM_INT);
                //print_r($_SESSION['username']." ".$survey_id." ".$question_id." ".$answer_id);exit;

                return $result->execute();
            }
        }

        return false;
    }

    public static function isCorrectAnswer($survey_id, $question_id, $answer_id)
    {
        $db = Db::getConnection();

        $correct_f = false;

        $sql = 'SELECT correct_f FROM answers
                      WHERE survey_id=:survey_id AND question_id=:question_id AND answer_id=:answer_id';

        $result = $db->prepare($sql);
        $result->bindParam(':question_id', $question_id, PDO::PARAM_INT);
        $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
        $result->bindParam(':answer_id', $answer_id, PDO::PARAM_INT);
        $result->execute();

        $row = $result->fetch();

        if (isset($row['correct_f']) && $row['correct_f'] == 1)
            $correct_f = $row['correct_f'];

        return $correct_f;

    }
}