<?php

class Questions
{
    public static function getQuestionItemById($survey_id, $question_id)
    {
        $db = Db::getConnection();

        $question = [];

        $sql = 'SELECT question_id, name, survey_id FROM questions 
                                        WHERE survey_id=:survey_id AND question_id=:question_id ORDER BY question_id';

        $result = $db->prepare($sql);
        $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
        $result->bindParam(':question_id', $question_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $questionItem = $result->fetch();

        return $questionItem;
    }

    public static function getQuestionListBySurveyId($survey_id)
    {
        $db = Db::getConnection();

        $questionList = [];

        $sql = 'SELECT question_id, name, survey_id FROM questions WHERE survey_id=:survey_id ORDER BY question_id';

        $result = $db->prepare($sql);
        $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $i = 0;

        while ($row = $result->fetch()) {
            $questionList[$i]['question_id'] = $row['question_id'];
            $questionList[$i]['name'] = $row['name'];
            $questionList[$i]['survey_id'] = $row['survey_id'];
            $i++;
        }

        return $questionList;
    }

    public static function getQuestionIdsBySurveyId($survey_id)
    {
        $db = Db::getConnection();

        $questionIds = [];

        $sql = 'SELECT question_id FROM questions WHERE survey_id=:survey_id ORDER BY question_id';

        $result = $db->prepare($sql);
        $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        while ($row = $result->fetch()) {
            $questionIds[] = $row['question_id'];
        }

        return $questionIds;
    }

    public static function getAnswerListByQuestionId($survey_id, $question_id)
    {
        $db = Db::getConnection();

        $answerList = [];

        $sql = 'SELECT answer_id, question_id, name FROM answers 
                                        WHERE question_id=:question_id AND survey_id=:survey_id 
                                        ORDER BY answer_id';

        $result = $db->prepare($sql);

        $result->bindParam(':question_id', $question_id, PDO::PARAM_INT);
        $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        while ($row = $result->fetch()) {
            $answerList[$row['answer_id']]['question_id'] = $row['question_id'];
            $answerList[$row['answer_id']]['name'] = $row['name'];
        }

        return $answerList;
    }

    public static function isLastQuestion($survey_id)
    {
        $db = Db::getConnection();

        $sql = 'SELECT max(question_id) as last_question_id FROM questions WHERE survey_id=:survey_id';

        $result = $db->prepare($sql);
        $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $last_question_id = $result->fetch()['last_question_id'];

        return $last_question_id;
    }
}