<?php

class Surveys
{
    public static function getSurveyList()
    {
        $db = Db::getConnection();

        $surveysList = [];

        $sql = 'SELECT name, survey_id FROM surveys ORDER BY name';

        $result = $db->prepare($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();


        while ($row = $result->fetch()) {
            $surveysList[$row['survey_id']] = $row['name'];
        }

        return $surveysList;
    }

    public static function getSurveySummary($survey_id)
    {
        $surveySummary = [];

        if (isset($_SESSION['username'])) {
            $db = Db::getConnection();

            $sql = 'SELECT username, count(answer_id) AS correct_answer_count, 
                (SELECT count(question_id) FROM questions WHERE survey_id=:survey_id) AS question_count 
                  FROM user_surveys WHERE username=:username AND survey_id =:survey_id AND correct_f=1 
                  GROUP BY username, survey_id';

            $result = $db->prepare($sql);
            $result->bindParam(':username', $_SESSION['username'], PDO::PARAM_STR);
            $result->bindParam(':survey_id', $survey_id, PDO::PARAM_INT);
            $result->execute();

            $surveySummary = $result->fetch();
        }

        return $surveySummary;
    }

    //delete summary if the same user name
    public static function deleteSurveySummary()
    {
        if (isset($_SESSION['username'])) {
            $db = Db::getConnection();

            $sql = 'DELETE FROM user_surveys WHERE username=:username';

            $result = $db->prepare($sql);
            $result->bindParam(':username', $_SESSION['username'], PDO::PARAM_STR);
            $result->execute();

            return $result->fetch();
        }

        return false;
    }
}