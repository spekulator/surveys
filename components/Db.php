<?php

class Db
{
    public static function getConnection(){
        $paramPath = __DIR__.'/../config/db_params.php';
        $params = include($paramPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        return $db;
    }
}