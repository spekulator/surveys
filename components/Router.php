<?php

class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = __DIR__ . '/../config/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * return request string
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        $uri = $this->getURI();
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                /*
                echo "<pre>";
                print_r($uriPattern);
                print_r($path);
                print_r($uri);
                echo "<br>";*/
                $segments = explode('/', $internalRoute);

                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $actionName = 'action' . ucfirst(array_shift($segments));

                $params = $segments;

                $controllerFile = __DIR__ . '/../controllers/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $params);

                if ($result != null) {
                    break;
                }

            }
        }
    }
}