-- --------------------------------------------------------
-- Хост:                         localhost
-- Версия сервера:               5.7.20 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных survey
CREATE DATABASE IF NOT EXISTS `survey` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `survey`;

-- Дамп структуры для таблица survey.answers
CREATE TABLE IF NOT EXISTS `answers` (
  `answer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `correct_f` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `answer_id` (`answer_id`),
  KEY `survey_id` (`survey_id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- Дамп данных таблицы survey.answers: 28 rows
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` (`answer_id`, `question_id`, `survey_id`, `name`, `correct_f`) VALUES
	(1, 1, 1, 'no brothers or sisters', NULL),
	(2, 1, 1, 'brothers or sisters', NULL),
	(38, 8, 1, 'There aren\'t any', NULL),
	(37, 8, 1, 'There is any', NULL),
	(36, 8, 1, 'There isn’t no', NULL),
	(35, 8, 1, 'There isn\'t any', 1),
	(34, 7, 1, 'didn’t have', 1),
	(33, 7, 1, 'didn’t have got', NULL),
	(32, 7, 1, 'no had', NULL),
	(31, 7, 1, 'hadn’t', NULL),
	(21, 1, 1, 'any brothers or sisters', 1),
	(22, 1, 1, 'some brothers and sisters', NULL),
	(23, 2, 1, 'a lot', NULL),
	(24, 2, 1, 'little', NULL),
	(25, 2, 1, 'too', NULL),
	(26, 2, 1, 'much', 1),
	(27, 6, 1, 'it', NULL),
	(28, 6, 1, 'him', 1),
	(29, 6, 1, 'her', NULL),
	(30, 6, 1, 'them', NULL),
	(39, 3, 2, '6', NULL),
	(40, 3, 2, '22', NULL),
	(41, 3, 2, '4', 1),
	(42, 4, 2, '9', 1),
	(43, 4, 2, '63', NULL),
	(44, 5, 2, '5', NULL),
	(45, 5, 2, '1', 1),
	(46, 5, 2, '0', NULL);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;

-- Дамп структуры для таблица survey.questions
CREATE TABLE IF NOT EXISTS `questions` (
  `question_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `survey_id` int(11) NOT NULL,
  PRIMARY KEY (`question_id`),
  KEY `question_id` (`question_id`),
  KEY `survey_id` (`survey_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Дамп данных таблицы survey.questions: 8 rows
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` (`question_id`, `name`, `survey_id`) VALUES
	(1, 'I haven\'t got ……', 1),
	(2, 'We haven’t got ..... Champagne', 1),
	(3, '2+2', 2),
	(4, '3*3', 2),
	(5, '5/5', 2),
	(6, 'David is the boss, you need to speak to …..', 1),
	(7, 'She …….. Supper with us last Friday', 1),
	(8, '..... sugar for my coffee!', 1);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;

-- Дамп структуры для таблица survey.surveys
CREATE TABLE IF NOT EXISTS `surveys` (
  `survey_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`survey_id`),
  KEY `survey_id` (`survey_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Дамп данных таблицы survey.surveys: 2 rows
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
INSERT INTO `surveys` (`survey_id`, `name`) VALUES
	(1, 'English test'),
	(2, 'Mathematic test');
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;

-- Дамп структуры для таблица survey.user_surveys
CREATE TABLE IF NOT EXISTS `user_surveys` (
  `user_survey_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(200) COLLATE latin1_bin NOT NULL,
  `survey_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `correct_f` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_survey_id`),
  UNIQUE KEY `uniq_index` (`username`,`survey_id`,`question_id`,`answer_id`),
  KEY `user_id` (`username`),
  KEY `surve_id` (`survey_id`),
  KEY `question_id` (`question_id`),
  KEY `answer_id` (`answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- Дамп данных таблицы survey.user_surveys: 0 rows
/*!40000 ALTER TABLE `user_surveys` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_surveys` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
