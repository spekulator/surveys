<?php
return array(
    'surveys' => 'survey/index',
    'survey/result/([0-9]+)' => 'survey/summary/$1',
    'questions/([0-9]+)/([0-9]+)' => 'question/view/$1/$2',
    'answers' => 'answer/add',
    'index' => 'survey/index',
    '' => 'survey/index'
);