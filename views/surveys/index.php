<?php include __DIR__ . '/../layouts/header.php'; ?>

    <h1>Testa uzdevums</h1>

    <div class="survey">

        <?php if (isset($errors) && is_array($errors)): ?>
            <?php foreach ($errors as $error): ?>
                <p class="error"> - <?php echo $error; ?></p>
            <?php endforeach; ?>
        <?php endif; ?>

        <form action="#" method="post">
            <input id="username" class="inputText" name="username" value="Ievadi savu vārdu" required>
            <br>
            <select id="survey_list" class="inputCombo" name="survey_id" required>
                <option disabled selected>Izvēlies testu</option>
                <?php foreach ($surveyList as $survey_id => $survey_name) : ?>
                    <option value="<?= $survey_id ?>"><?= $survey_name ?></option>
                <?php endforeach; ?>
            </select>

            <p>
                <input class="inputSubmit" type="submit" value="Sākt">
            </p>
        </form>

    </div>

<?php include __DIR__ . '/../layouts/footer.php'; ?>