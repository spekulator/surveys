<?php include __DIR__ . '/../layouts/header.php'; ?>

    <div class="survey">

        <h1>Paldies, <?=$surveySummary['username']?>!</h1>
        <h2>Tu atbildēji pareizi uz <?=$surveySummary['correct_answer_count']?> no <?=$surveySummary['question_count']?> jautājumiem.</h2>

    </div>

<?php include __DIR__ . '/../layouts/footer.php'; ?>