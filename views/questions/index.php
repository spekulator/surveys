<?php include __DIR__ . '/../layouts/header.php'; ?>

<h1><?= $questionItem['name'] ?></h1>
<div class="survey">
    <form action="#" method="post">
        <?php foreach ($answerList as $answer_id => $v) : ?>
            <input id="q<?=$answer_id?>" data_ids="<?=$survey_id.'|'.$v['question_id'].'|'.$answer_id?>" class="inputAnswerSubmit" type="submit" value="<?= $v['name'] ?>">
        <?php endforeach; ?>
    </form>
    <br>
    <div id="progressbar">
        <div></div>
    </div>
    <br>
    <form action="../../questions/<?= $survey_id ?>/<?= $next_question_id ?>" method="post">
        <input class="inputSubmit" type="submit" value="Nākamais">
    </form>
</div>

<style>
    #progressbar div {
        width: <?=$progressbar_val?>%;
    }
</style>


<?php include __DIR__ . '/../layouts/footer.php'; ?>
