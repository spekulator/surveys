<?php
//General params
ini_set('display_errors', 1);
error_reporting(E_ALL);

session_start();

require_once(__DIR__ . '/components/Autoload.php');

//Router call
$router = new Router();
$router->run();
