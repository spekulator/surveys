<?php

class SurveyController
{
    public function actionIndex()
    {
        $surveyList = [];
        $surveyList = Surveys::getSurveyList();

        //print_r($surveyList);
        //print_r($_POST);

        $errors = false;

        if ($_POST) {

            if (!isset($_POST['survey_id']) || empty($_POST['survey_id']))
                $errors[] = "Lūdzu izvēlieties testu";
            if (!isset($_POST['username']) || empty($_POST['username']))
                $errors[] = "Lūdzu ievadiet vārdu";
        }

        if (isset($_POST['survey_id']) && empty($errors)) {
            $survey_id = $_POST['survey_id'];

            $questionIds = [];
            $questionIds = Questions::getQuestionIdsBySurveyId($survey_id);

            //print_r($questionIds);

            if (isset($questionIds[0])) {
                $_SESSION['username'] = trim($_POST['username']);
                header("Location: ../questions/$survey_id/{$questionIds[0]}");
            }
        } else {
            if (isset($_SESSION['username'])) {
                unset($_SESSION['username']);
                session_destroy();
            }
        }

        require_once(__DIR__ . '/../views/surveys/index.php');

        return true;
    }
}