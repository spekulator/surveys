<?php
/**
 * Created by PhpStorm.
 * User: jz
 * Date: 04.10.2018
 * Time: 23:37
 */

class AnswerController
{
    public function actionAdd()
    {
        if (isset($_POST['data'])) {
            $params = explode('|', $_POST['data']);

            if (isset($_SESSION['username']) && count($params) == 3) {
                $survey_id = (int)$params[0];
                $question_id = (int)$params[1];
                $answer_id = (int)$params[2];

                Answers::addAnswerItem($survey_id, $question_id, $answer_id);

                return true;
            }
        }

        return false;
    }
}