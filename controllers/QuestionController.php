<?php

class QuestionController
{
    /*
    public function actionIndex($survey_id)
    {
        $questionList = [];
        $questionList = Questions::getQuestionListBySurveyId($survey_id);

        require_once(__DIR__ . '../views/index.php');

        echo '<pre>';
        print_r($_POST);
        print_r($questionList);
        echo '</pre>';

        return true;
    }*/

    public function actionView($survey_id, $question_id)
    {
        $questionIds = [];
        $questionIds = Questions::getQuestionIdsBySurveyId($survey_id);
        //var_dump(array_search($question_id, $questionIds)===false);
        if (array_search($question_id, $questionIds) === false
            && isset($_SESSION['username'])) {

            $surveySummary = [];
            $surveySummary = Surveys::getSurveySummary($survey_id);

            if (!$surveySummary) {
                $surveySummary['username'] = $_SESSION['username'];
                $surveySummary['correct_answer_count'] = 0;
                $surveySummary['question_count'] = count($questionIds);
            }

            require_once(__DIR__ . '/../views/surveys/summary.php');

            unset($_SESSION['username']);
            session_destroy();
            return true;
        }

        $questionItem = [];
        $questionItem = Questions::getQuestionItemById($survey_id, $question_id);

        $answerList = [];
        $answerList = Questions::getAnswerListByQuestionId($survey_id, $question_id);

        $next_question_id = null;
        $progressbar_val = 0;
        if (array_search($question_id, $questionIds) == 0){
            Surveys::deleteSurveySummary();
        }
        //print_r($questionIds);
        //print_r($_SESSION);

        $progressbar_val = ((array_search($question_id, $questionIds) + 1) / count($questionIds)) * 100;
        if (isset($questionIds[array_search($question_id, $questionIds) + 1])) {
            $next_question_id = $questionIds[array_search($question_id, $questionIds) + 1];
        } else
            $next_question_id = ++$question_id;

        require_once(__DIR__ . '/../views/questions/index.php');

        return true;
    }
}